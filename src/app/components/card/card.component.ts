import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() description: string;
  @Input() options: string[] = [];
  @Input() backgroundColor: string = '#fb0000';
  @Output() cardHandlerButton: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  cardHandler(option: string) {
    this.cardHandlerButton.emit(`se clickeo la option: ${option} del card ${this.description}`);
  }

}
