import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showCards: boolean = true;
  albums: any[] = [
    { description: 'Descripción 1', options: [ 'Editar' ], backgroundColor: '#ffffff' } ,
    { description: 'Descripción 2', options: [ 'Editar', 'Eliminar' ], backgroundColor: '#00bcd4' } ,
    { description: 'Descripción 3', options: [ 'Editar' ], backgroundColor: '#00bcd4' } ,
  ] 

  get lastAlbum() {
    return this.albums[this.albums.length - 1]
  }

  toggleShowCards(): void {
    this.showCards = !this.showCards;
  }

  addText(description: string): string {
    return `${description} añadido`
  }

  log(text: string) {
    console.log(text);
  }

}
